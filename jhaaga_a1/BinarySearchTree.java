// Joseph Haaga Assignment 1 part 2

import edu.gwu.algtest.*;
import edu.gwu.debug.*;
import edu.gwu.util.*;

public class BinarySearchTree implements TreeSearchAlgorithm {

  public static edu.gwu.algtest.TreeNode root;
  public static int size=0;

  
  public edu.gwu.algtest.TreeNode getRoot(){
    return root;
  }

  
  public java.lang.Object insert(Comparable key, Object val){ 
    // if tree is empty,
    if(size==0){
      root = new TreeNode(key, val);
      size = size+1;
      return root;
    }else if(search(key)!=null){
      recursiveInsert(root,key,val);
    }else{
      recursiveInsert(root, key, val);
      return true;
    }
    return false;
  }

  // 
  public java.lang.Object recursiveInsert(edu.gwu.algtest.TreeNode node, Comparable<Object> key, Object val){
    if(key.compareTo(node.key)==-1){
      if(node.left!=null){
        recursiveInsert(node.left,key,val);
      }else{
        node.left = new TreeNode(key, val);
        // node.left.left=null;
        // node.left.right=null;
        size++;
      }
    }else if(key.compareTo(node.key)==1){
      if(node.right!=null){
        recursiveInsert(node.right,key,val);
      }else{
        node.right = new TreeNode(key, val);
        // node.right.left=null;
        // node.right.right=null;
        size++;
      }
    }else{
      Object old_val = node.value;
      node.value = val;
      // size++;
      return old_val;
    }
    return false;
  }
      
  
  public edu.gwu.algtest.ComparableKeyValuePair search(java.lang.Comparable key){
    boolean found = false;
    TreeNode temp = root;
    int instruction;
    while(!found){
      if(temp==null){
        return temp;
      }
      instruction=key.compareTo(temp.key);
      // -1 : <
      if(instruction==-1){
        // go left
        temp=temp.left;
      }else if(instruction==0){
        return temp;
      }else{
        temp=temp.right;
      }
    }
    return root;
  }


public java.lang.Comparable successor(java.lang.Comparable key){
    return null;
  }

// 
public java.lang.Comparable predecessor(java.lang.Comparable key){
    return null;
  }

  // 
  public java.lang.Object delete(java.lang.Comparable key){
    // node is leaf
    boolean found = false;
    ComparableKeyValuePair c = search(key);
    if(c!=null){
      // if key exists in BST
      recursiveDelete(key,root);
    }else{
      // key doesn't exist; delete failed
      return false;
    }
    return false;
  }


  // 
  public java.lang.Object recursiveDelete(java.lang.Comparable key, edu.gwu.algtest.TreeNode node){
 // if target < current key go left
    if(key.compareTo(node.key)==-1){
      // go left
      recursiveDelete(key,node.left);
    }else if(key.compareTo(node.key)==1){
      // go right
      recursiveDelete(key,node.right);
    }else{
      // node is target; initiate 3 delete cases
      // NO CHILDREN
      if(node.left==null && node.right==null){   // if node is a leaf AKA no children
        if(node.parent.left!=null){ 
          if(node.parent.left.key.compareTo(key)==0){ 
            node.parent.left = null;
          }else{
            node.parent.right = null;
          }
          return node;
        }else{                        // temp node is right child of parent
          if(node.parent.right.key.compareTo(key)==0){
            node.parent.right = null;
          }else{
            node.parent.left = null;
          }
          return node;
        }
      // ONE CHILD
      }else if(node.left == null){  // if node has a right child only...
          if(node.parent.left.key == key){ // node is left child of parent...
            node.parent.left = node.right;
          }else{                           // node is right child of parent...
            node.parent.right = node.right;
          }
          return node;
      }else if(node.right==null){   // node has left child only
          if(node.parent.left.key == key){ // node is left child of parent...
            node.parent.left = node.left;
          }else{                           // node is right child of parent...
            node.parent.right = node.left;
          }
          return node;
      // TWO CHILDREN
      }else{

      }
    }
    return false;
  }


  
public ComparableKeyValuePair maximum(){
  if (getMax(root) == null){
    return null; 
  }
  ComparableKeyValuePair maximumPair = new ComparableKeyValuePair(getMax(root).key , getMax(root).value);
  return maximumPair; 
}


private TreeNode getMax(TreeNode node){
  while (node.right != null){ 
    node = node.right;
  }
  return node;
}

public ComparableKeyValuePair minimum(){  
  if (getMin(root) == null){ 
    return null; 
  } 
  ComparableKeyValuePair minPair = new ComparableKeyValuePair(getMin(root).key , getMin(root).value);  
  return minPair; 
}

private TreeNode getMin(TreeNode node){
  while (node.left != null){
   node = node.left; 
  }
  return node; 
}

  /* ----------------- SearchAlgorithm methods ---------------- */
  
  public void initialize(int maxSize){
      size=0;
      TreeNode root = new TreeNode();
      size++;
  }

  
  public int getCurrentSize(){
    return 0;
  }

  
  public java.util.Enumeration<Object> getKeys(){
    return null;
  }
  
  public java.util.Enumeration<Object> getValues(){
    return null;
  }

  
  public java.lang.String getName(){
    return "Joseph Haaga's Implementation of BinarySearchTree";
  }

  
  public void setPropertyExtractor(int algID, edu.gwu.util.PropertyExtractor prop){

  }

  public static void main (String[] args) {
      
  }


}