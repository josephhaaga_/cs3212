import edu.gwu.algtest.*;
import edu.gwu.debug.*;
import edu.gwu.util.*;
import edu.gwu.geometry.*;
import java.util.ArrayList;

/*

Joseph Haaga, CS3212, Fall 2015,  Assignment 2 Part 2

*/

public class FilterTreeRectIntersection implements RectangleSetIntersectionAlgorithm{
	FilterTreeNode root;

	public String getName(){
		return "Joseph Haaga's FilterTree implementation for Rectangle Intersections";
	}

	public void makeFilterTree(FilterTreeNode n, IntRectangle[] rectSet){
		n = buildTree(4,0,10000);
		for(IntRectangle r:rectSet){
			recursive_insert(r,n);
		}
	}

	@Override
	public void setPropertyExtractor(int algID, PropertyExtractor prop){
		
	}

	public IntPair[] findIntersections(IntRectangle[] rectSet1, IntRectangle[] rectSet2){
		int total=0;
		ArrayList<IntPair> intersections = new ArrayList<IntPair>();
		IntRectangle first = rectSet1[0];
		FilterTreeNode root = new FilterTreeNode(first.topLeft.x,first.bottomRight.x,first.bottomRight.y,first.topLeft.y,1);
		makeFilterTree(root,rectSet1);
		for(IntRectangle i:rectSet1){
			for(IntRectangle j:rectSet2){
				if(doTheyIntersect(i,j)==1){
					intersections.add(new IntPair(i.getID(),j.getID()));
					total++;
				}
			}

		}
		if(total==0){
			return null;
		}
		IntPair[] resultintersections = new IntPair[intersections.size()];
		for(int i=0;i<intersections.size();i++){
			resultintersections[i] = (IntPair) intersections.get(i);
		}		
		return  resultintersections;
		// return null;
	}	

	public static void printTree(FilterTreeNode root){
		IntRectangle r; 
		String existing_string = "";
		StringBuilder builder = new StringBuilder(existing_string);
		for (int i = 0; i < root.level; i++) {
		    builder.append("  ");
		}
		System.out.println(builder.toString()+"level:"+root.level + " " + root.rectList);
		if(!root.isLeaf){
			for(FilterTreeNode q:root.quadrants){
				printTree(q);
			}
		}
		for(int i=0;i<root.rectList.size();i++){
			// r = (IntRectangle)root.rectList.get(i);
			// System.out.println("\nRectangle "+r.getID()+"; topLeft:("+r.topLeft.x+","+r.topLeft.y+"), bottomRight:("+r.bottomRight.x+","+r.bottomRight.y+")");
		}
	}

	public static int doTheyIntersect(IntRectangle r1, IntRectangle r2){
		if((r1.topLeft.x <= r2.topLeft.x) && (r1.topLeft.x  <= r1.bottomRight.x)){
			if((r1.topLeft.y >= r2.topLeft.y) && (r2.topLeft.y  >= r1.bottomRight.y)){
			return 1;
		}else if((r1.topLeft.y >= r2.bottomRight.y) && (r2.bottomRight.y  >= r1.bottomRight.y)){
			return 1;
		}
		}else if((r1.topLeft.x <= r2.bottomRight.x) && (r1.bottomRight.x  <= r1.bottomRight.x)){
			if((r1.topLeft.y >= r2.topLeft.y) && (r2.topLeft.y  >= r1.bottomRight.y)){
			return 1;
		}else if((r1.topLeft.y >= r2.bottomRight.y) && (r2.bottomRight.y  >= r1.bottomRight.y)){
			return 1;
		}
		}
		return 0;
	}

// 	(18,70)(32,60)
	public static int doesRectangleIntersectQuadrantBisectors(FilterTreeNode n, IntRectangle r){
		if((r.topLeft.x <= n.midX) && (n.midX <= r.bottomRight.x)){
			// System.out.println("Rectangle intersects mid-x");
			return 1;
		}else if((r.topLeft.y >= n.midY) && (n.midY >= r.bottomRight.y)){
			// System.out.println(r.topLeft.y+">="+n.midY+">="+r.bottomRight.y);
			return 1;
		}
		return 0;
	}	

	public static int determineQuadrant(IntRectangle query_rectangle, FilterTreeNode root){
		int number = -1;
		// determine middle of rectangle and solve for quadrant
		int middleX = (query_rectangle.topLeft.x + query_rectangle.bottomRight.x)/2;
		int middleY = (query_rectangle.topLeft.y + query_rectangle.bottomRight.y)/2;
		if((middleX>root.midX)&&(middleY>root.midY)){
			// quadrant 0 
			number = 0;
		}else if((middleX<root.midX) && (middleY>root.midY)){
			// quadrant 1
			number = 1;
		}else if((middleX<root.midX)&&(middleY<root.midY)){
			// quadrant 2
			number = 2;
		}else{
			number = 3;
		}
		return number;
	};


	public static FilterTreeNode buildTree(int levels, int min, int max){
		FilterTreeNode root = new FilterTreeNode(min,max,max,min,1);
		int depth = 1;
		return createQuadrants(levels,depth,root);
	}

	public static FilterTreeNode createQuadrants(int levels,int depth,FilterTreeNode r){
		depth++;
		r.quadrants[0]=new FilterTreeNode(r.midX,r.rightX,r.midY,r.topY,depth);
		r.quadrants[1]=new FilterTreeNode(r.leftX,r.midX,r.midY,r.topY,depth);
		r.quadrants[2]=new FilterTreeNode(r.leftX,r.midX,r.botY,r.midY,depth);
		r.quadrants[3]=new FilterTreeNode(r.midX,r.rightX,r.botY,r.midY,depth);
		if(levels>depth){
			// System.out.println("calling createQuadrants again  level:" +depth);
			createQuadrants(levels,depth,r.quadrants[0]);
			createQuadrants(levels,depth,r.quadrants[1]);
			createQuadrants(levels,depth,r.quadrants[2]);
			createQuadrants(levels,depth,r.quadrants[3]);
		}else{
			// System.out.println("not working");
			r.quadrants[0].isLeaf=true;
			r.quadrants[1].isLeaf=true;
			r.quadrants[2].isLeaf=true;
			r.quadrants[3].isLeaf=true;
		}

		return r;
	}
/*
PART 1 PSEUDOCODE

Recursive insert(pointsToInsert, filterTree )
	If recursiveSearch == true, break; // already in filterTree
	Else
		Check if x/2 is within bounds of pointstoInsert X1 and x2
			True=> insert at this level  
		Check if y/2 is within bounds of pointsToInsert Y1 and Y2
			True=> insert @ this level
		Else
Determine quadrant(s) containing rectangleToInsert and call RecursiveInsert on them 
*/



	public static void recursive_insert(IntRectangle rectToInsert, FilterTreeNode n){
		if(recursive_search(rectToInsert,n)!=null){
			// System.out.println("Already in tree");
		}else if(doesRectangleIntersectQuadrantBisectors(n, rectToInsert)==1){
			// rectangle belongs to this level
			// System.out.println("BISECTS "+n.level);
			n.rectList.add(rectToInsert);
		}else if(n.isLeaf){
			n.rectList.add(rectToInsert);
			// System.out.println("\n Rectangle"+rectToInsert+" inserted at "+n.level);
		}else{
			int quad = determineQuadrant(rectToInsert, n);
			if(quad!=-1){
				recursive_insert(rectToInsert,n.quadrants[quad]);
			}
		}
	}


/*
PART 1 PSEUDOCODE

Recursive search(queryPoints, filterTree)
	Check intersection w points in current filterTree node 
	If filterTree has pointers to children 
		Determine quadrant(s) containing queryPoints and call recursive search(queryPoints, quadrant)
	Else
Return null otherwise
*/

	
	public static IntRectangle recursive_search(IntRectangle query_rectangle, FilterTreeNode root){
		IntRectangle p = null;	

		if(root.rectList.contains(query_rectangle)){
			return (IntRectangle)root.rectList.get(root.rectList.indexOf(query_rectangle));
		}else if(!root.isLeaf){
			int quad = determineQuadrant(query_rectangle, root);
			p = recursive_search(query_rectangle,root.quadrants[quad]);
		}
		return p;
	}

	public static void main(String args[]){
        IntRectangle[] testArray=new IntRectangle[5];
        // level 1
        IntRectangle rectA=new IntRectangle(12,51,43,37);
        // level 2
        IntRectangle rectB=new IntRectangle(66,80,66,86);
        doTheyIntersect(rectA,rectB);
        FilterTreeNode f = buildTree(3,0,100);
        

		IntRectangle r1 = new IntRectangle(4,79,19,69);
		IntRectangle r2 = new IntRectangle(19,69,39,49);
		IntRectangle r3 = new IntRectangle(19,29,39,19);
		// set 2
		IntRectangle r4 = new IntRectangle(69,79,79,69);
		IntRectangle r5 = new IntRectangle(29,54,59,24); 
		// IntRectangle r6 = new IntRectangle(49,29,64,19); 
		// IntRectangle r7 = new IntRectangle(69,29,79,19);

        // IntRectangle r1 = new IntRectangle(new IntPoint(89,57), new IntPoint(91,37)); 
        r1.ID=1;
		// IntRectangle r2 = new IntRectangle(new IntPoint(12,80), new IntPoint(45,66));
		r2.ID=2;
		// IntRectangle r3 = new IntRectangle(new IntPoint(28,40), new IntPoint(39,29)); 
		r3.ID=3;
		// IntRectangle r4 = new IntRectangle(new IntPoint(18,70), new IntPoint(32,60)); 
		r4.ID=4;
		// IntRectangle r5 = new IntRectangle(new IntPoint(79,94), new IntPoint(82,90));
		r5.ID=5;
        System.out.println("\n ========== CONSTRUCTING TREE ========= \n");
        recursive_insert(r1,f);
        System.out.println("\n -"+r1+" inserted \n");
        recursive_insert(r2,f);
        System.out.println("\n -"+r2+" inserted \n");
        recursive_insert(r3,f);
        System.out.println("\n -"+r3+" inserted \n");
        recursive_insert(r4,f);
        System.out.println("\n -"+r4+" inserted \n");
        recursive_insert(r5,f);
        System.out.println("\n -"+r5+" inserted \n");
        System.out.println("\n ========== PRINTING TREE ========= \n");
        printTree(f);
        IntRectangle [] listr1 = new IntRectangle[5];
        listr1[0]=r1;
        listr1[1]=r2;
        listr1[2]=r3;
        listr1[3]=r4;
        listr1[4]=r5;
        IntRectangle [] listr2 = new IntRectangle[5];
        listr2[0]=r2;
        listr2[1]=r2;
        listr2[2]=r2;
        listr2[3]=r2;
        listr2[4]=r2;

        FilterTreeRectIntersection l = new FilterTreeRectIntersection();
        System.out.println("\n ========== TESTING recursive_search ========= \n");
        System.out.println("\n Search for R1");
        System.out.println(" expected:"+r1+" \n actual:"+recursive_search(r1,f));
        System.out.println("\n Search for R2");
        System.out.println(" expected:"+r2+" \n actual:"+recursive_search(r2,f));
        System.out.println("\n Search for R3");
        System.out.println(" expected:"+r4+" \n actual:"+recursive_search(r4,f));
        System.out.println("\n Search for R4");
        System.out.println(" expected:"+r4+" \n actual:"+recursive_search(r4,f));
        System.out.println("\n Search for R5");
        System.out.println(" expected:"+r5+" \n actual:"+recursive_search(r5,f));
		System.out.println("\n\n\n ========== TESTING findIntersections ========= \n");
		System.out.println("\n");
		int count=1;
		long epoch = System.currentTimeMillis();
		System.out.println("Size of results:"+l.findIntersections(listr1,listr2).length);
		for(IntPair p:l.findIntersections(listr1,listr2)){

			System.out.println(count++ + "\t"+p.toString());
		}
		System.out.println("End:"+((System.currentTimeMillis())-epoch)+"ms");
	}
}