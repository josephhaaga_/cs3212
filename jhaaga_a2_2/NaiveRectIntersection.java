// Joseph Haaga, CS3212, Fall 2015,  Assignment 2 Part 1
import java.util.ArrayList;
import edu.gwu.algtest.*;
import edu.gwu.debug.*;
import edu.gwu.util.*;
import edu.gwu.geometry.*;

// Iterate through the first set of rectangles, comparing each rectangle R1 with each R2 in the second set.
//		If any R2 has an x-boundary within the constraints of R1's x-boundaries...
//			Check if R2 has a y boundary within constraints of R1's y-boundaries...
//				If True: R1 and R2 Intersect!
// 		Else if R1 is entirely contained in R2...

public class NaiveRectIntersection implements RectangleSetIntersectionAlgorithm{

	// Boilerplate
	public String getName(){
		return "Joseph Haaga's Naive Rectangle Intersection Algorithm";
	}
	@Override
	public void setPropertyExtractor(int i, PropertyExtractor p){
		
	}

	// Part 1 Question 2
	@Override
	public IntPair[] findIntersections(IntRectangle[] set_one, IntRectangle[] set_two){
		ArrayList<IntPair> results = new ArrayList<IntPair>();
		int count=0;
		for(IntRectangle r1: set_one){
			for(IntRectangle r2: set_two){
				if((r1.topLeft.x <= r2.topLeft.x) && (r2.topLeft.x<= r1.bottomRight.x)){
					// x's overlap; topLeft X of r2 is with-in bounds of r1's topLeft X and bottomRight X
					if(r2.topLeft.y < r1.bottomRight.y || r2.bottomRight.y > r1.topLeft.y){
						// NOT INTERSECTING
					}else{
						results.add(new IntPair(r1.getID(),r2.getID()));
						count++;
					}
				}else if((r1.topLeft.x <= r2.bottomRight.x) && (r2.bottomRight.x <= r1.bottomRight.x)){
					// x's overlap; bottomRight X of r2 is with-in bounds of r1's topLeft X and bottomRight X
					if(r2.topLeft.y < r1.bottomRight.y || r2.bottomRight.y > r1.topLeft.y){
						// NOT INTERSECTING
					}else{
						results.add(new IntPair(r1.getID(),r2.getID()));
						count++;
					}
				}else if(r2.topLeft.x <= r1.topLeft.x && r2.bottomRight.x >= r1.bottomRight.x){
					// fringe case r1 is entirely within r2
					results.add(new IntPair(r1.getID(),r2.getID()));
					count++;
				}
			}
		}
		IntPair[] filteredResults = new IntPair[results.size()];
		for(int i=0;i<results.size();i++){
			filteredResults[i] = results.get(i);
		}
		// return results;
		if(results.size()==0){
			return null;
		}
		return filteredResults;
	}

	// main method
	public static void main(String args[]){
		// set 1
		IntRectangle r1 = new IntRectangle(5,80,20,70);
		IntRectangle r2 = new IntRectangle(20,70,40,50);
		IntRectangle r3 = new IntRectangle(20,30,40,20);
		// set 2
		IntRectangle r4 = new IntRectangle(70,80,80,70);
		IntRectangle r5 = new IntRectangle(30,55,60,25); 
		IntRectangle r6 = new IntRectangle(50,30,65,20); 
		IntRectangle r7 = new IntRectangle(70,30,80,20);
	
		IntRectangle[] s1 = {r1,r2,r3};
		IntRectangle[] s2 = {r4,r5,r6,r7};

		long epoch = System.currentTimeMillis();

		NaiveRectIntersection g= new NaiveRectIntersection();

		IntPair[] results = g.findIntersections(s1,s2);


		System.out.println("========================================================================");
		for(IntPair p: results){
			System.out.println(p);
		}

		System.out.println("End:"+(epoch-(System.currentTimeMillis()))+" ms");
	}

}