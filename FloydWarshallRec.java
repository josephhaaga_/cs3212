// Floyd-Warshall algorithm - recursive version (for illustration)

import java.util.*;
import java.io.*;


public class FloydWarshallRec {

  // Debugging for internal use.
  private static final boolean debug = false;
  
  int numVertices;                   // Number of vertices (when initialized).

  double[][] adjMatrix;              // The adjacency matrix (given as input).

  double[][] D;                      // Matrix used in dynamic programming.


  public void initialize (int numVertices)
  {
    this.numVertices = numVertices;

    // Initialize D matrices.
    D = new double [numVertices][];
    for (int i=0; i < numVertices; i++){
      D[i] = new double [numVertices];
    }
  }

  
  // Recursive computation of D. k is the "k" in "S_k".

  double computeD (int k, int i, int j) 
  {
    // INSERT YOUR CODE HERE.
  }


  public void allPairsShortestPaths (double[][] adjMatrix)
  {
    this.adjMatrix = adjMatrix;
    
    for (int i=0; i<numVertices; i++) {
      for (int j=0; j<numVertices; j++) {
        if (i != j) {

          D[i][j] = computeD (numVertices-1, i, j);

        }
      }
    }

    // ... (path construction not shown) ...

  }
  
  
  public static void main (String[] argv)
  {
    // A test case.
      double[][] adjMatrix = {
        {0, 1, 7, 0, 5, 0, 1},
        {1, 0, 1, 0, 0, 7, 0},
        {7, 1, 0, 1, 7, 0, 0},
        {0, 0, 1, 0, 1, 0, 0},
        {5, 0, 7, 1, 0, 1, 0},
        {0, 7, 0, 0, 1, 0, 1},
        {1, 0, 0, 0, 0, 1, 0},
      };

      int n = adjMatrix.length;
      FloydWarshallRec fwAlg = new FloydWarshallRec ();
      fwAlg.initialize (n);
      fwAlg.allPairsShortestPaths (adjMatrix);
      
      // ... print paths (not shown) ...

  }

}