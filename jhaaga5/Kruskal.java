import edu.gwu.algtest.*;
import edu.gwu.util.*;
import java.lang.*;
import java.text.*;
import java.util.*;


/*
Joseph Haaga; CSCI3212 Exercise 5 Part 2 Extra Credit
In this part, you will use Kruskal's algorithm (with adjacency matrices) to estimate the minimum-spanning tree weight of a randomly-created set of points. The points will be n random points in the unit square.

*/
public class Kruskal implements SpanningTreeAlgorithm{
	int algID;
	PropertyExtractor prop;
	double[][] treeMatrix; // do we need a global?
	int nVertices;
	UnionFindInt u;
	DecimalFormat df;
	ArrayList<GraphEdge> edgeList;
    double totalWeight;
    double averageWeight;
    

	public double[][] minimumSpanningTree(double[][] adjMatrix){
		double[][] mstMatrix = new double[nVertices][nVertices]; 
		// weights = new LinkedList<Double>();
		// add edges to edgeList
		totalWeight=0;
		for(int i=0;i<adjMatrix.length;i++){
            for(int j=i;j<adjMatrix.length;j++){
                if (adjMatrix[i][j]>0){
                	totalWeight+=adjMatrix[i][j];
                    edgeList.add(new GraphEdge(i,j,adjMatrix[i][j]));
                }
            }
        }
        averageWeight = totalWeight /edgeList.size();
        ArrayList<GraphEdge> sortedEdgeList = new ArrayList<GraphEdge>();

        // for(int i=0;i<adjMatrix.length;i++){

        // }

        Collections.sort(edgeList, new Comparator<GraphEdge>(){
        	@Override public int compare(GraphEdge g1, GraphEdge g2){
        		int solution=0;
        		if(g1.weight-g2.weight>0){
        			solution = 1;
        		}else if(g1.weight-g2.weight<0){
        			solution = -1;
        		}
        		return solution;
        	}
        });

        System.out.println();
        for(GraphEdge g : edgeList){
        	// System.out.print(g.weight+"\t");
        }
        GraphEdge tempEdge;
        treeMatrix = new double[nVertices][nVertices];
	// 6.   Initialize treeMatrix to store MST;
        Iterator p = edgeList.iterator();
        while(p.hasNext()){
        	tempEdge = (GraphEdge) p.next();
        	if(u.find(tempEdge.startVertex)!=u.find(tempEdge.endVertex)){
        		u.union(tempEdge.startVertex, tempEdge.endVertex);
        		treeMatrix[tempEdge.startVertex][tempEdge.endVertex] = adjMatrix[tempEdge.startVertex][tempEdge.endVertex];
        		treeMatrix[tempEdge.endVertex][tempEdge.startVertex] = adjMatrix[tempEdge.startVertex][tempEdge.endVertex];
        	}
        }
		return treeMatrix;
	// 14.  return treeMatrix
	}
	public GraphVertex[] minimumSpanningTree(GraphVertex[] adjList){
		return null;
	}

	public void initialize(int numVertices){
		u=new UnionFindInt();
		edgeList=new ArrayList<GraphEdge>();
		df= new DecimalFormat("0.00");
		u.initialize(numVertices);
		nVertices = numVertices;
		treeMatrix = new double[nVertices][nVertices];
		for(int i=0; i<numVertices; i++){
			u.makeSingleElementSet(i);
		}
	}
	
	public double getTreeWeight(){
		return totalWeight;
	}

	public String getName(){
		return "Joseph Haaga's Implementation of Kruskal";
	}

	public void setPropertyExtractor(int algID, edu.gwu.util.PropertyExtractor p){
		this.prop=p;
		this.algID = algID;
	}

	public void printMatrix(double[][] adjMatrix){
		System.out.println("\nPrinting matrix...\n");
		for(int i=0; i<adjMatrix.length; i++){
			for(int j=0; j<adjMatrix.length; j++){
				System.out.print(df.format(adjMatrix[i][j])+"\t");
			}
			// System.out.print("\n");
		}
	}

	public static void main(String args[]){
		UniformRandom r = new UniformRandom();
		Kruskal k = new Kruskal();
		double average = 0.0;

		// for(int p=0;p<100;p++){
			k.initialize(10);
			// generate a test adjacency matrix of a connected graph
			double[][] testMatrix = new double[10][10];

			for(int i=0; i<10; i++){
				for(int j=0; j<10; j++){
					if(r.uniform()<0.4){
						testMatrix[i][j] = r.uniform();
						testMatrix[j][i] = testMatrix[i][j];
					}
				}			
			}
			k.printMatrix(testMatrix);
			double[][] z= k.minimumSpanningTree(testMatrix);
			k.printMatrix(z);
			System.out.println("\n\t weight: "+k.getTreeWeight());
			average+=k.getTreeWeight();
		// }
		// System.out.println("\nAverage over 100 trials: "+average/100);

	}
}
