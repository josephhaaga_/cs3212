import edu.gwu.algtest.*;
import edu.gwu.util.*;

public class UnionFindInt{
	int numberOfElements;
	int[] componentList;

	public void initialize(int number){
		numberOfElements = number;
		componentList = new int[number];
	}

	public void union(int val1, int val2){
		if(componentList[val1]<componentList[val2]){
            for (int i=componentList.length-1;i>=0;i--){
                if(componentList[i]==componentList[val2]){
                    componentList[i]=componentList[val1];
                }
            }
        }else{
            for (int i=componentList.length-1;i>=0;i--){
                if(componentList[i]==componentList[val1]){
                    componentList[i]=componentList[val2];
                }
            }
        }
	}

	public void makeSingleElementSet(int z){
		componentList[z]=z;
	}

	public int find(int v){
		return componentList[v];
	}

	public static void main(String args[]){
		UnionFindInt n = new UnionFindInt();
		n.initialize(6);
		for(int p : n.componentList){
			// System.out.print("\t"+n.find(p));
		}
	}

}