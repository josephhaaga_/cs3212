// Joseph Haaga, CS3212, Fall 2015, Assignment 3 Part 2
import edu.gwu.algtest.*;
import edu.gwu.debug.*;
import edu.gwu.util.*;
import edu.gwu.geometry.*;
import java.lang.*;
import java.util.Collections;
import java.util.LinkedList;
// Annealing algorithm for Travelling Salesman problem

public class Annealing implements MTSPAlgorithm{
	int algID;
    double temperature = 100.0;
    int NUMBER_OF_SALESMEN=0;
	PropertyExtractor prop;     
     
    public void printTest(int[][] p){
        for(int i=0;i<p.length;i++){
            for(int j=0;j<p[i].length;j++){
                System.out.print(" "+p[i][j]);
            }
            System.out.println("");
        }
    }
    
    public int indexOfPoint(Pointd p, Pointd[] points){
        int count=0;
        for(int i=0;i<points.length;i++){
            if(p.equals(points[i])){
                return count;
            }
            count++;
        }
        return -1;
        
    }
    
    public double computeCost(Pointd a, Pointd b){
        return Math.sqrt(((b.getx()-a.getx())*(b.getx()-a.getx())+((b.gety())-a.gety())*(b.gety()-a.gety())));
    }
    
    public int[][] computeTours(int m, Pointd[] points){
        int[][] assignedPoints=new int[m][];
        int averageNum=points.length/m;
        Naive bad=new Naive();
        int localCounter=0;
        for(int t=0;t<m-1;t++){
            assignedPoints[t]=new int[averageNum];
            localCounter+=averageNum;
        }
        assignedPoints[m-1]=new int[points.length-localCounter];
        assignedPoints=bad.computeTours(m,points);
        int[][]bestMatrix=assignedPoints;
        double bestCost=1000001.0;
        for(double i=0;i<1000000.0;i++){
            int[][] testMatrix=new int[m][];
            int localCount=0;
            for(int t=0;t<m-1;t++){
                testMatrix[t]=new int[averageNum];
                localCount+=averageNum;
            }
            testMatrix[m-1]=new int[points.length-localCount];
            Pointd[] pt=new Pointd[points.length];
            for(int o=0;o<pt.length;o++){
                pt[o]=points[o];
            }
            LinkedList<Pointd> temporaryPoints=new LinkedList<Pointd>();
            while(temporaryPoints.size()<pt.length){
                int rand=(int)(Math.random()*points.length);
                if(pt[rand]!=null){
                    temporaryPoints.add(pt[rand]);
                    pt[rand]=null;
                }
            }
            for(int k=0;k<testMatrix.length;k++){
                for(int n=0;n<testMatrix[k].length;n++){
                    testMatrix[k][n]=indexOfPoint(temporaryPoints.poll(),points);
                }
            }
            double testCost=totalCost(testMatrix,points);
            if(testCost<bestCost){
                bestCost=testCost;
                bestMatrix=testMatrix;
            }
        }
        return bestMatrix;
    }
    
    public double totalCost(int[][] pointMatrix,Pointd[] points){
        int[] costs=new int[pointMatrix.length];
        for(int i=0;i<costs.length;i++){
            costs[i]=0;
        }
        double total=0.0;
        for(int i=0;i<pointMatrix.length;i++){
            for(int j=1;j<pointMatrix[i].length;j++){
                costs[i]+=computeCost(points[pointMatrix[i][j-1]],points[pointMatrix[i][j]]);
            }
        }
        for(int i=0;i<costs.length;i++){
            total+=costs[i];
        }
        return total;
    }
    
// ====================== BOILER PLATE===========================
	public void setPropertyExtractor(int algID, PropertyExtractor p){
		this.prop=p;
		this.algID = algID;
	}
	public String getName(){
		return "Joseph Haaga's Annealing Travelling Salesman";
	}

	public static void main(String args[]){
		// generate input - 4 salesmen, 10 points
		Pointd[] points = new Pointd[10];
		int NUMBER_OF_SALESMEN = 4;
		for(int i=0;i<10;i++){
			points[i] = new Pointd( Math.random()*10, Math.random()*10);
		//	System.out.println("Point "+i+": "+points[i].x+", "+points[i].y);
		}
		Annealing n = new Annealing();
		System.out.println("++++++++++ Printing Annealing  +++++++++++");	
		int[][] results = n.computeTours(NUMBER_OF_SALESMEN, points);
		for(int l=0;l<NUMBER_OF_SALESMEN;l++){
			System.out.println("\nSalesman "+l+": ");
			for(int m=0;m<results[l].length;m++){
				System.out.print(results[l][m]+" ");
			}
		}	
	}
	
} 


 
