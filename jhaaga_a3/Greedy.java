// Joseph Haaga, CS3212, Fall 2015, Assignment 3 Part 2
import edu.gwu.algtest.*;
import edu.gwu.debug.*;
import edu.gwu.util.*;
import edu.gwu.geometry.*;
import java.lang.*;
import java.util.Collections;
import java.util.LinkedList;
// Greedy algorithm for Travelling Salesman problem

public class Greedy implements MTSPAlgorithm{
	int algID;
	PropertyExtractor prop;     
     
    public void printTest(int[][] p){
        for(int i=0;i<p.length;i++){
            for(int j=0;j<p[i].length;j++){
                System.out.print(" "+p[i][j]);
            }
            System.out.println("");
        }
    }
    
    public Pointd closestNeighbor(Pointd start,LinkedList<Pointd> list){
        Pointd closestNeighbor=null;
        double returnCost=1000001.0;
        LinkedList<Pointd>copy=new LinkedList<Pointd>();
        for(Pointd y:list){
            copy.add(y);
        }
        int count =0;
        while(copy.size()!=0){
            Pointd query=copy.poll();
            if(computeCost(start,query)<returnCost){
                closestNeighbor=query;
                returnCost=computeCost(start, query);
            }
        }
        return closestNeighbor;
    }
    
    public int indexOfPoint(Pointd p, Pointd[] points){
        int count=0;
        for(int i=0;i<points.length;i++){
            if(p.equals(points[i])){
                return count;
            }
            count++;
        }
        return -1;
        
    }
    
    public int indexOfPointInList(Pointd p, LinkedList<Pointd> list){
        LinkedList<Pointd> copy=new LinkedList<Pointd>();
        for(Pointd y:list){
            copy.add(y);
        }
        int count=0;
        Pointd inquiry=copy.peek();
        while(copy.size()>0){
            inquiry=copy.peek();
            if(p.y==inquiry.y && p.x==inquiry.x){
                return count;
            }
            inquiry=copy.poll();
            count++;
        }
        return -2;
    }
    
    public int[][] computeTours(int m, Pointd[] points){
        int[][] assignedPoints=new int[m][];
        double minDistance=computeCost(points[0],points[1]);
        int overAllSpacesFilled=0;
        int averageNum=points.length/m;
        int localCounter=0;
        for(int t=0;t<m-1;t++){
            assignedPoints[t]=new int[averageNum];
            localCounter+=averageNum;
        }
        assignedPoints[m-1]=new int[points.length-localCounter];
        LinkedList<Pointd> pointList=new LinkedList<Pointd>();
        for(int i=0;i<points.length;i++){
            pointList.add(points[i]);
        }
       
        Pointd currentPoint=points[0];
        
        for(int i=0;i<m-1;i++){
            int numSpacesFilled=0;
            while(numSpacesFilled<assignedPoints[i].length){
                if(currentPoint==points[0]){
                    assignedPoints[i][numSpacesFilled]=0;
                    numSpacesFilled++;
                }
                int index=indexOfPointInList(currentPoint,pointList);
                Pointd trash=pointList.remove(index);
                Pointd closestNeighbor=closestNeighbor(currentPoint, pointList);
                assignedPoints[i][numSpacesFilled]=indexOfPoint(closestNeighbor,points);
                numSpacesFilled++;
                overAllSpacesFilled++;
                currentPoint=closestNeighbor;
            }
            
        }
       int numSpacesFilled=0;
        while(pointList.size()>1){
            int index=indexOfPointInList(currentPoint,pointList);
            Pointd trash=pointList.remove(index);
            Pointd closestNeighbor=closestNeighbor(currentPoint, pointList);
            assignedPoints[m-1][numSpacesFilled]=indexOfPoint(closestNeighbor,points);
            numSpacesFilled++;
            overAllSpacesFilled++;
            currentPoint=closestNeighbor;
        }
        return assignedPoints;
        
    }
    
    public double computeCost(Pointd a, Pointd b){
        return Math.sqrt(((b.getx()-a.getx())*(b.getx()-a.getx())+((b.gety())-a.gety())*(b.gety()-a.gety())));
    }
    
// ====================== BOILER PLATE===========================
	public void setPropertyExtractor(int algID, PropertyExtractor p){
		this.prop=p;
		this.algID = algID;
	}
	public String getName(){
		return "Joseph Haaga's Greedy Travelling Salesman";
	}

	public static void main(String args[]){
		// generate input - 4 salesmen, 10 points
		Pointd[] points = new Pointd[10];
		int NUMBER_OF_SALESMEN = 4;
		for(int i=0;i<10;i++){
			points[i] = new Pointd( Math.random()*10, Math.random()*10);
		//	System.out.println("Point "+i+": "+points[i].x+", "+points[i].y);
		}
		Greedy n = new Greedy();
		System.out.println("++++++++++ Printing Greedy  +++++++++++");	
		int[][] results = n.computeTours(NUMBER_OF_SALESMEN, points);
		for(int l=0;l<NUMBER_OF_SALESMEN;l++){
			System.out.println("\nSalesman "+l+": ");
			for(int m=0;m<results[l].length;m++){
				System.out.print(results[l][m]+" ");
			}
		}	
	}
	
} 


 
