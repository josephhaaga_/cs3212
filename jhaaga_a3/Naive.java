// Joseph Haaga, CS3212, Fall 2015, Assignment 3 Part 2
import edu.gwu.algtest.*;
import edu.gwu.debug.*;
import edu.gwu.util.*;
import edu.gwu.geometry.*;

// Naive algorithm for Travelling Salesman problem

public class Naive implements MTSPAlgorithm{
	int algID;
	PropertyExtractor prop;

	public void printArray(int[] arr){
		System.out.println();
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}


	public int[][] computeTours(int m, Pointd[] points){
		int ctr = 0;
		int size=(int)points.length/m;
		int[] lens = new int[m];
		int remainder = points.length % m;
		int[][]  result = new int[m][points.length/m];
		if(remainder > 0) {
			size++;
			for(int k=0;k<remainder;k++){
				result[k] = new int[(int)size];
			}	
		} 

		
		while(ctr<points.length){
			result[ctr%m][lens[ctr%m]++]=ctr;	
	//		System.out.print(" "+ctr);
			ctr++;
		}
	//	for(int j=0;j<result.length;j++){
	//		printArray(result[j]);
	//	}	
		return result;
	}
// ====================== BOILER PLATE===========================
	public void setPropertyExtractor(int algID, PropertyExtractor p){
		this.prop=p;
		this.algID = algID;
	}
	public String getName(){
		return "Joseph Haaga's Naive Travelling Salesman";
	}

	public static void main(String args[]){
		// generate input - 4 salesmen, 10 points
		Pointd[] points = new Pointd[10];
		int NUMBER_OF_SALESMEN = 4;
		for(int i=0;i<10;i++){
			points[i] = new Pointd( Math.random()*10, Math.random()*10);
			System.out.println("Point "+i+": "+points[i].x+", "+points[i].y);
		}
		Naive n = new Naive();
		System.out.println("++++++++++ Printing Naive +++++++++++");	
		int[][] results = n.computeTours(NUMBER_OF_SALESMEN, points);
		for(int l=0;l<NUMBER_OF_SALESMEN;l++){
			System.out.println("\nSalesman "+l+": ");
			for(int m=0;m<results[l].length;m++){
				System.out.print(results[l][m]+" ");
			}
		}	
	}
	
} 


 
