
import edu.gwu.algtest.*;
import edu.gwu.debug.*;
import edu.gwu.util.*;
import java.util.ArrayList;
import java.util.Random;

public class MergeSortOpt implements SortingAlgorithm {

// overriding the SortingAlgorithm's function with Joe Haaga's MergeSortOpt
public void sortInPlace(int [] data){
  int cutoff=60000;
  if(data.length<=cutoff){
    SelectionSort is = new SelectionSort();
    is.sortInPlace(data);
  }else{
  ArrayList<int[]> l = new ArrayList<int[]>();
  //  splitting step: each item in input array is placed in its own array inside master ArrayList
    for(int i=0; i<data.length; i++){
      int[] n = new int[]{data[i]};
      l.add(n);
    }

    // e.g. [ [12], [9], [8], [5]  ]
    int index = 0;
    // while we have more than one array in our ArrayList...
    while(l.size()>1){
      // merge 2 newest
      int[] temp = merge(l.get(0),l.get(1));
      l.add(temp);
      l.remove(0);
      l.remove(0);
    }
   
    System.out.println();
    System.out.println("After Merge Sort: ");
    printMerge(l.get(0));
  }
}

  public void printMerge(int[] data){
      System.out.println("----------------------------");
      for(int i=0; i<data.length;i++){
        System.out.print(" "+data[i]+",");
      }
    System.out.println();
  }
     
  public static int[] merge(int[] a, int[] b){
     int i=0;
     int j=0;
     int z=0;
     int[] result = new int[a.length + b.length];
     // while arrays A or B still have elements...
     while (i<a.length && j<b.length){
        // add the next element in each list to the result array in correct order
        if(a[i]<b[j]){
          result[z]=a[i];
          z++;
          i++;
        }else{
          result[z]=b[j];
          z++;
          j++;
        }
        
     }


     // add the leftovers
     while (i<a.length){
      result[z]=a[i];
      z++;
      i++;
     }

     while(j<b.length){
      result[z] = b[j];
      z++;
      j++;
     }
     return result;
  }  


  public void sortInPlace(java.lang.Comparable[] data){
  //sortInPlace sorts object arrays that meet the Comparable interface so that comparisons may be done by called the compareTo method of the objects.
  }

  
  public int [] createSortIndex(int [] data){
  //Instead of modifying the order of data in the original array, createSortIndex should create and return 
  //an array of indices into the original array in sort order. Thus, if data[0]=10, data[1]=15, data[2]=5 the returned array should contain 2, 0, 1.
  return null;
  }
        
 
  public int [] createSortIndex(java.lang.Comparable[] data){
  //createSortIndex should return a sort index for Comparable
  return null;
  }
  
  public java.lang.String getName(){
    return ("Joe Haaga's MergeSortOpt");
  //getName should return your name and the name of your algorithm, e.g., "Beavis' implementation of MergeSortOpt".
  }

  
  public void setPropertyExtractor(int algID, edu.gwu.util.PropertyExtractor prop){
  //setPropertyExtractor will be called by the simulator with an instance of 
  //PropertyExtractor which an algorithm can use to extract properties from the original properties file.
  }
  

  public static void main (String[] args) {
   
    System.out.println();
    System.out.println("==================== TEST 1 ====================");
    System.out.println();
     System.out.println();


   //Create tests to make sure the merge works.
    int[] data = {10,3,12,9,8,5};

    System.out.println("Before Merge Sort: ");
    
    MergeSortOpt insrt = new MergeSortOpt();
    insrt.printMerge(data);
    insrt.sortInPlace(data);

    System.out.println();
     System.out.println();
    System.out.println("==================== TEST 2 ====================");
    System.out.println();
     System.out.println();



    /* Test the sort with 10 elements to see if the result is sorted and that 
    no elements were overwritten or "lost" in the sorting. */

    int[] data2 = {10,4,2,5,3,9,7,6,1,8};

    System.out.println("Before Merge Sort: ");
    
    MergeSortOpt insrt2 = new MergeSortOpt();
    insrt2.printMerge(data2);
    insrt2.sortInPlace(data2);


    System.out.println();
     System.out.println();
    System.out.println("==================== TEST 3 ====================");
    System.out.println("=========== 100 Random numbers in [0,100]===========");
    System.out.println();
     System.out.println();
      System.out.println();

    /* Test the sort with 10 elements to see if the result is sorted and that 
    no elements were overwritten or "lost" in the sorting. */

    int[] data3 = new int[100];
    Random randomGenerator = new Random();

    for(int l=0; l<100; l++){
      data3[l] = randomGenerator.nextInt(100);
    }
    System.out.println("Before Merge Sort: ");
    
    MergeSortOpt insrt3 = new MergeSortOpt();
    insrt2.printMerge(data3);
    insrt2.sortInPlace(data3);

  }
  
}