
public class ex2 {

  static char[] generateChars (int length)
  {
    char[] charArray = new char [length];
    for (int i=0; i<length; i++) {
      charArray[i] = (char) UniformRandom.uniform ( (long)'A', (long) 'C' );
    }
    return charArray;
  }
  

  static int findFirstOccurrence (char[] pattern, char[] text)
  {
    int d =32;
    // compute pattern signature
    int p = 0;
    int patternSignature=0;
    while(p<pattern.length){
      patternSignature+=(int)pattern[p];
      p++;
    }
    // initialize current text signature to first substring of length m.
    p=0;
    int textHash = 0;
    while(p<pattern.length){
      textHash+=(int)text[p];
      p++;
    }
    // no match possible after this position
    int lastPossiblePosition = text.length - pattern.length;
    int textCursor =0;
    while(textCursor<=lastPossiblePosition){
      if(textHash==patternSignature){
        // evaluate match
            int l=0;
            int i=0;
            //check each char text[l] against pattern[i]
            while(l<text.length){
              if(text[l]==pattern[i]){
                if(i==pattern.length-1){
                  return l;
                }
                l++;
                i++;
              }else{
                l++;
              }
            }
      }
      textCursor++;
      textHash = textHash - ((int)text[textCursor-1]) + ((int)text[textCursor-1+pattern.length]);
    }
    return -1;
  }

  public static void main (String[] argv)
  {
    try {
      char[] text = null;
      char[] pattern = null;
      if (argv.length != 2) {
        String textStr = "ABABACBABABA";
        String patternStr = "BAC";
        text = textStr.toCharArray();
        pattern = patternStr.toCharArray();
      }
      else {
        int textLength = Integer.parseInt (argv[0].trim());
        int patternLength = Integer.parseInt (argv[1].trim());
        text = generateChars (textLength);
        pattern = generateChars (patternLength);
      }
      
      int position = findFirstOccurrence (pattern, text);
      if (position >= 0)
        System.out.println ("Found at position=" + position);
      else
        System.out.println ("Not found");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  

}
