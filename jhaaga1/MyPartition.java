import edu.gwu.algtest.*;
import edu.gwu.debug.*;
import edu.gwu.util.*;

public class MyPartition implements PartitionAlgorithm{

	public int leftIncreasingPartition(int[] data, int left, int right){
	    int pivot = 0;
	    for(int i = 1; i <= right;i++){
		    if(data[pivot] >= data[pivot+1]){
		    	int temp = data[pivot+1];
		        data[pivot+1] = data[pivot];
		        data [pivot] = temp;
		        pivot++;
		    }
		    else{
		    	int temp = data[pivot+1];
		        for(int j = pivot + 1; j < right; j++ ){
		        	data[j] = data[j+1];
		        }
		      	data[right] = temp;
		    }
		}
		return pivot;
	}


	//rightIncreasingPartition is just leftIncreasingPartition except with data[right]
	public int rightIncreasingPartition(int[] data, int left, int right){
		return 0;
	}

      //for the Algorithm interface, a parent of the SortingAlgorithm inerface

 	public java.lang.String getName(){
 		return "Joseph Haaga's Partition Implementation";
 	}

	public void setPropertyExtractor(int algID,edu.gwu.util.PropertyExtractor prop){
		//empty implementation, method definition with empty body
	}

}

