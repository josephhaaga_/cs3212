import edu.gwu.algtest.*;
import edu.gwu.debug.*;
import edu.gwu.util.*;

public class InsertionSort implements SortingAlgorithm {

  static int rangeLow = 100;
  static int rangeHigh = 1000;
  private String name;
  private String algorithm;


        @Override
public void sortInPlace(int [] data){
  //sortInPlace should sort the array in place
  int i;
  int j;
  int temp;
            
        for (i = 1; i <data.length; i++){
            j = i;
            while (j>0 && data[j-1] > data[j]){
                temp = data[j];
                data[j] = data[j-1];
                data[j-1] = temp;
                j--;
            }
      }
}
  

        @Override
  public void sortInPlace(java.lang.Comparable[] data){
  //sortInPlace sorts object arrays that meet the Comparable interface so that comparisons may be done by called the compareTo method of the objects.
  }

        @Override
  public int [] createSortIndex(int [] data){
            
  //Instead of modifying the order of data in the original array, createSortIndex should create and return 
  //an array of indices into the original array in sort order. Thus, if data[0]=10, data[1]=15, data[2]=5 the returned array should contain 2, 0, 1.
  return null;
  }

        @Override
  public int [] createSortIndex(java.lang.Comparable[] data){
  //createSortIndex should return a sort index for Comparable
            return null;
  }


  @Override
    public java.lang.String getName(){
    name = "jhaaga";
    algorithm = " Insertion Sort";
    return (name + "'s" + algorithm);
  //getName should return your name and the name of your algorithm, e.g., "Beavis' implementation of SelectionSort".
  }

        @Override
  public void setPropertyExtractor(int algID, edu.gwu.util.PropertyExtractor prop){
  //setPropertyExtractor will be called by the simulator with an instance of 
  //PropertyExtractor which an algorithm can use to extract properties from the original properties file.
  }
        
  

  public static void main (String[] args) {
   
    int[] data = {12,9,76,48,13,8,32,26};

    System.out.println("Before Insertion Sort: ");
    
    for(int i:data){
        System.out.print(i);
        System.out.print(", ");
    }
   
    int i;
    InsertionSort insrt = new InsertionSort();
    insrt.sortInPlace(data);
    
    System.out.println();
    System.out.println("After Insertion Sort: ");
           
    for(i = 0; i< data.length; i++){
            System.out.print(data[i]);
            System.out.print(", ");
    }
  }
}