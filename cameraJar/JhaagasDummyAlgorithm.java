import java.util.*;

public class JhaagasDummyAlgorithm implements CameraPlacementAlgorithm {

    // Algorithm should solve and return list of Camera instances
    // as camera locations and orientations.

    public ArrayList<Camera> solve (CameraPlacementProblem problem) 
    {
	// INSERT YOUR CODE HERE
    	ArrayList<Camera> cameraList = new ArrayList<>();

	// Place cameras at two corners, with aligned outer-viewing limits (blue line in GUI
	Camera camera = new Camera( new Pointd(1,1),  30, true );
	cameraList.add(camera);
	camera = new Camera( new Pointd(1,problem.getMaxY()-1), 300, true);
	cameraList.add(camera);
	//camera = new Camera( new Pointd(problem.getMaxX()-1, 1), 135, false );
	//cameraList.add(camera);
	//camera = new Camera( new Pointd(problem.getMaxX()-1, problem.getMaxY()-1), 225, false );
	//cameraList.add(camera);

    	return cameraList;
    }
}
