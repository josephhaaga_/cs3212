import java.util.*;
import java.lang.*;

public class JhaagasAlgorithm implements CameraPlacementAlgorithm {

    // Algorithm should solve and return list of Camera instances
    // as camera locations and orientations.
	
	public int determineQuadrant(Pointd p, int maxX, int maxY){
		if(p.x<maxX/2){
			if(p.y<maxY/2){
				return 3-1;
			}else{
				return 2-1;
			}
		}else{
			if(p.y<maxY/2){
				return 4-1;
			}else{
				return 1-1;
			}
		}
		
	}

    public int[] countInteriorWalls(CameraPlacementProblem prob){
		// n(quadrant#) represents number of interior walls in a quadrant
		// return [n(I),n(II),n(III),n(IV)]
		int[] result = new int[4];
		int maxX = prob.getMaxX();
		int maxY = prob.getMaxY();
		// for every wall...
		int p1quad, p2quad, midquad = 0;
		ArrayList<Wall> wallList= prob.getInteriorWalls();
		for(int i=0;i<wallList.size();i++){
			// determine endpoint (and midpoint) quadrants
			p1quad = determineQuadrant(wallList.get(i).start, maxX,maxY);
			p2quad = determineQuadrant(wallList.get(i).end,maxX,maxY);
//			midquad = determineQuadrant(wallList.get(i).start);	
			// increment appropriate quadrants in result array
			result[p1quad]++;
			result[p2quad]++;	
		}
		return result;
	}

	public int calculateAngle(int targetQuad,Integer[] quadrantWalls){
		int result =0;
		switch(targetQuad){
			case 0:
				if(quadrantWalls[1]>quadrantWalls[3]){
					result = 240;
				}else{
					result = 210;
				}
				break;
			case 1:
				if(quadrantWalls[0]>quadrantWalls[2]){
					result = 300;
				}else{
					result = 330;
				}
				break;
			case 2:
				if(quadrantWalls[1]>quadrantWalls[3]){
					result = 30;
				}else{
					result = 60;
				}
				break;
			case 3:
				if(quadrantWalls[2]>quadrantWalls[0]){
					result = 120;
				}else{
					result = 150;
				}
				break;
		}
		return result;
	}

    public ArrayList<Camera> solve (CameraPlacementProblem problem) 
    {

	// INSERT YOUR CODE HERE
	// Count number of interior walls in each quadrant [I,II,III,IV]
	int[] quadrantWallCounts = countInteriorWalls(problem);
    	ArrayList<Camera> cameraList = new ArrayList<>();
	Integer[] quadrantWalls = Arrays.stream( quadrantWallCounts ).boxed().toArray( Integer[]::new );
	for(int i=0;i<4;i++){System.out.print(quadrantWallCounts[i]+" ");}
	int targetQuadrant = Arrays.asList(quadrantWalls).indexOf(Collections.min(Arrays.asList(quadrantWalls)))+1;
	// Place cameras at two corners, with aligned outer-viewing limits (blue line in GUI
	double x1 =0;
	double y1 = 0;
	int angle1 = 0;
	System.out.println("target quadrant: "+targetQuadrant+"\n");
	// pick corner for 1st camera
	switch(targetQuadrant){
		case 1: 
			x1 = problem.getMaxX()-1;
			y1 = problem.getMaxY()-1;
			break;
		case 2: 
			x1 = 1;
			y1 = problem.getMaxY()-1;
			// calculate angle
			break;
		case 3: 
			x1 = 1;
			y1 = 1;
			// calculate angle
			break;
		case 4: 
			x1 = problem.getMaxX()-1;
			y1 = 1;
			// calculate angle
			break;
	}
	angle1 = calculateAngle(targetQuadrant-1,quadrantWalls);
	Camera camera = new Camera( new Pointd(x1,y1),  angle1, true );
	cameraList.add(camera);
//	camera = new Camera( new Pointd(problem.getMaxX()-1,problem.getMaxY()-1), 210, true);
//	cameraList.add(camera);

    	return cameraList;
    }
}
