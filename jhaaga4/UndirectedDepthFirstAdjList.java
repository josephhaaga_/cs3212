import edu.gwu.algtest.*;
import edu.gwu.util.*;
import edu.gwu.debug.*;
import java.util.*;
// Joseph Haaga's UndirectedDepthFirstAdjList.java
// Algorithms CS3212 Exercise 4

public class UndirectedDepthFirstAdjList implements UndirectedGraphSearchAlgorithm{
	int algID;
	PropertyExtractor prop;
	LinkedList<GraphEdge>[] adjList;
	int nVertices;
	int nConnectedComponents;
	int[] componentLabel;
	int currentComponentLabel;
	boolean weighted;

	int visitCount,completionCount;
	int[] dfsVisitOrder;
	int[] dfsCompletionOrder;

	public void initialize(int numVertices, boolean isWeighted){
		this.weighted=isWeighted;
		componentLabel = new int[numVertices];
		// initially each vertex stands alone.
		for(int z=0;z<numVertices;z++){
			componentLabel[z]=z;
		}
		this.nVertices = numVertices;
		adjList = new LinkedList[nVertices];
		// RESET ALL VARS
		for(int i=0;i<numVertices;i++){
			adjList[i] = new LinkedList<GraphEdge>();
		}
		// System.out.println("Initialized "+numVertices+" vertices.\n");
		nConnectedComponents = nVertices;
	} 

	public void insertUndirectedEdge(int startVertex, int endVertex, double weight){
		if(!weighted){
			weight=1.0;
		}
		if(adjList[startVertex].contains(new GraphEdge(startVertex,endVertex,weight))){
			return;
		}else{
			// not in adj list
			adjList[startVertex].addLast(new GraphEdge(startVertex,endVertex,weight));
			adjList[endVertex].addLast(new GraphEdge(endVertex,startVertex,weight));
		}

		// System.out.println("\n"+startVertex+"---"+endVertex+" inserted.\n");
	}

	public boolean existsOddCycle(){
		return false;
	}

	public boolean existsCycle(){
		return false;
	}

	public int[] breadthFirstVisitOrder(){
		return null;
	}

	public int[] breadthFirstArticulationOrder(){
		return null;
	}

	public int[] componentLabels(){
		System.out.println("Component Labels:");
		for(int i=0; i<componentLabel.length;i++){
			System.out.print(componentLabel[i]);
		}
		return componentLabel;
	}

	public int[] depthFirstCompletionOrder(){
	// 	// return array of vertices in order of completion
		// int[] result = new int[nVertices];

	// 	// complete DFS search to populate Visit order 
	// 	depthFirstVisitOrder();

	// 	for(int k=0;k<dfsCompletionOrder.length;k++){
	// 		result[k]=dfsCompletionOrder.get(k);
	// 	}
		return dfsCompletionOrder;
	}

	public int[] depthFirstVisitOrder(){

		visitCount = -1;
    	completionCount = -1;

    	dfsVisitOrder = new int[nVertices];
    	dfsCompletionOrder = new int[nVertices];
    	currentComponentLabel=-1;


	    for (int i=0; i < nVertices; i++) {
	      dfsVisitOrder[i] = -1;
	      dfsCompletionOrder[i] = -1;
	    }

	    for (int i=0; i < nVertices; i++) {
	      if (dfsVisitOrder[i] < 0) {
	      	currentComponentLabel = currentComponentLabel+1;
	        recurseDFS(i);
	      }
	    }
	    return dfsVisitOrder;

		// dfsCompletionOrder = new ArrayList<Integer>();
		// dfsVisitOrder = new ArrayList<Integer>();
		// int[] result = new int[nVertices];
		// System.out.println("result array is length "+result.length);
		// int temp=0;
		// while(dfsVisitOrder.size()<nVertices){
		// 	System.out.println("dsfVisitOrder count:"+temp);
		// 	recurseDFS(temp);
		// 	temp++;
		// 	System.out.println("dsfVisitOrder returned "+temp);
		// }
		// for(int k=0;k<dfsVisitOrder.size();k++){
		// 	result[k]=dfsVisitOrder.get(k);
		// }
		// return result;
	}


	public void recurseDFS(int v){

    // 1. First, visit the given vertex. Note: visitCount is a global. 
    visitCount++;
    dfsVisitOrder[v] = visitCount;

    componentLabel[v] = currentComponentLabel;

     for(int i=0; i<adjList[v].size(); i++){
     	if(adjList[v].get(i)!=null){
     		if(dfsVisitOrder[adjList[v].get(i).endVertex]<0){
     			recurseDFS(adjList[v].get(i).endVertex);
     		}
     	}
     }

    // 3. After returning from recursion(s), set the post-order or "completion" order number. 
    completionCount++;
    dfsCompletionOrder[v] = completionCount;
		// // label v discovered
		// if(!dfsVisitOrder.contains(v)){
		// 	dfsVisitOrder.add(v);
		// 	// System.out.println("added "+v+" to the recurseDFS results");
		// }
		// // for edges from v
		// LinkedList<Integer> j = adjList.get(v);
		// while(j.iterator().hasNext()){
		// 	int k = j.pop();
		// 	if(!dfsVisitOrder.contains(k)){
		// 		recurseDFS(k);
		// 	}
		// }
		// dfsCompletionOrder.add(v);
	}

	public int[] articulationVertices(){
		return null;
	}

	public int numConnectedComponents(){
		List<Integer> l = new ArrayList<Integer>();
		for(int i=0;i<componentLabel.length;i++){
			if(!l.contains(componentLabel[i])){
				l.add(componentLabel[i]);
			}
		}
		return l.size();
	}

	public GraphEdge[] articulationEdges(){
		return null;
	}

	public String getName(){
		return "Joseph Haaga's Undirected DFS w/ adjacency list";
	}

	// @Override
	public void setPropertyExtractor(int algID,PropertyExtractor prop){
		this.algID = algID;
		this.prop = prop;
	}

	public static void main(String args[]){


		/*		Testing 	 */
		UndirectedDepthFirstAdjList u = new UndirectedDepthFirstAdjList();
		u.initialize(8,false);
		u.insertUndirectedEdge(0,2,3.0);
		u.insertUndirectedEdge(0,5,3.0);
		u.insertUndirectedEdge(1,4,3.0);
		u.insertUndirectedEdge(2,0,3.0);
		u.insertUndirectedEdge(2,3,3.0);
		u.insertUndirectedEdge(3,1,3.0);
		u.insertUndirectedEdge(3,4,3.0);
		u.insertUndirectedEdge(5,2,3.0);
		u.insertUndirectedEdge(5,6,3.0);
		u.insertUndirectedEdge(5,7,3.0);
		u.insertUndirectedEdge(6,7,3.0);
		u.insertUndirectedEdge(7,5,3.0);
		// System.out.println("\n adjList.get(nVerts).get(i)= \n"+u.adjList.get(1).get(0));
		System.out.println("visit order:\n");
		int[] result = u.depthFirstVisitOrder();
		for(int k:result){
			System.out.print(k+",");
		}
		System.out.println("\n\n");

		// System.out.println("Completion order:\n");
		// int[] result2 = u.depthFirstCompletionOrder();
		// for(int k:result2){
		// 	System.out.print(k+",");
		// }
		// System.out.println("\n# Connected Components:"+u.numConnectedComponents());
		// System.out.println("\n======== Testing 0 edges =======");
		// u.initialize(8,false);
		// System.out.println("# Connected Components:"+u.numConnectedComponents());
		// System.out.println("\n======== Testing 1 edges =======");
		// u.initialize(8,false);
		// u.insertUndirectedEdge(0,1,3.0);
		// u.depthFirstVisitOrder();
		// System.out.println("# Connected Components:"+u.numConnectedComponents());
		// System.out.println("\n======== Testing 3 edges =======");
		// u.initialize(8,false);
		// u.insertUndirectedEdge(0,1,3.0);
		// u.insertUndirectedEdge(2,1,3.0);
		// u.insertUndirectedEdge(0,4,3.0);
		// u.depthFirstVisitOrder();
		// System.out.println("# Connected Components:"+u.numConnectedComponents());
		// System.out.println("====== TESTING 2 VERTICES ======");
		// u.initialize(2,false);
		// u.insertUndirectedEdge(0,1,0);
		// u.insertUndirectedEdge(0,0,0);
		// System.out.println("Completion order:");
		// // int[] result3 = u.depthFirstCompletionOrder();
		// for(int k:result3){
		// 	System.out.print(k+" ");
		// }

		// // System.out.println("\nExpected completion Order:");
		// int[] arrayOfInt3 = { 1, 0 };
		// for(int l:arrayOfInt3){
		// 	System.out.print(l+" ");
		// }
		// u.depthFirstVisitOrder();
		// System.out.println("\n# Connected Components:"+u.numConnectedComponents());

		// PART 3: graph.png (red = 10 vertex graphs; blue = 20vertexgraphs) 
		// generate thousands of random graphs to estimate avg # of connected components for different p values
		// 		p is probability that two vertices connect

		double[] ten_vertex_scores= new double[21];
		double p=0.1;
		double average =0;
		for(int i=0;i<21;i++){
			for(int j=0;j<51;j++){
				u.initialize(10,false);
				for(int z=0;z<10;z++){
					for(int k=0;k<10;k++){
						if (UniformRandom.uniform() < p){
							// System.out.println("EDGE INSERTEED");
					      u.insertUndirectedEdge(z,k,0.0);
						}
	  				    else{
	  				    	// System.out.print("not connected");
	  				    }
					}
				}
				u.depthFirstVisitOrder();
				// System.out.println(u.numConnectedComponents()+" components");
				average+=u.numConnectedComponents();
			}
			ten_vertex_scores[i]=average/50;
			average =0.0;
			p=p+0.05;
		}

		double[] twenty_vertex_scores= new double[21];
		p=0.1;
		average =0;
		for(int i=0;i<21;i++){
			for(int j=0;j<51;j++){
				u.initialize(20,false);
				for(int z=0;z<20;z++){
					for(int k=0;k<20;k++){
						if (UniformRandom.uniform() < p){
							// System.out.println("EDGE INSERTEED");
					      u.insertUndirectedEdge(z,k,0.0);
						}
	  				    else{
	  				    	// System.out.print("not connected");
	  				    }
					}
				}
				u.depthFirstVisitOrder();
				// System.out.println(u.numConnectedComponents()+" components");
				average+=u.numConnectedComponents();
			}
			twenty_vertex_scores[i]=average/50;
			average =0.0;
			p=p+0.05;
		}

		// int[] ten_vertex_scores = new int[1000];
		// // 10 vertices
		// double p=0.1;
		// System.out.println("\n++++Ten Vertex Graph++++\n");
		// for(int i=0;i<1000;i++){
		// 	// System.out.println("\nNew 10 vertex Graph");
		// 	u.initialize(10,false);
		// 	if(i%100==0){
		// 		p=p+0.1;
		// 	}
		// 	for(int j=0;j<10;j++){
		// 		for(int k=0;k<10;k++){
		// 			if (UniformRandom.uniform() < p){
		// 		      u.insertUndirectedEdge(j,k,0.0);
		// 			}
  // 				    else{
  // 				    	// System.out.print("not connected");
  // 				    }
		// 		}
		// 	}
		// 	u.depthFirstVisitOrder();
		// 	// System.out.println("Connected Components:"+u.numConnectedComponents());
		// 	ten_vertex_scores[i]=u.numConnectedComponents();
		// }

		System.out.println("10 vertex Scores:");
		for(int k=0;k<ten_vertex_scores.length;k++){
			System.out.print(ten_vertex_scores[k]+" ");
		}

		System.out.println("\n\n20 vertex Scores:");
		for(int k=0;k<twenty_vertex_scores.length;k++){
			System.out.print(twenty_vertex_scores[k]+" ");
		}



	}
}